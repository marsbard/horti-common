package com.bettercode.horti.common;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

public class TimeUtil {
	
	private static final Logger log = Logger.getLogger(TimeUtil.class);

	/**
	 * Check that the supplied time value matches the specified format, for example, 
	 * initial format during building is 'hhmm0' but will probably change to 'YYYY-MM-DD hh:mm:ss'
	 * @param format
	 * @param value
	 * @throws HortiException
	 */
	public static boolean checkTimeFormat(String format, String value) throws HortiException {
		String regex = "^";
		
		for( int i = 0; i < format.length(); i++){
			char c = (char) format.getBytes()[i];
			if(c=='h' || c=='m' || c=='Y' || c=='M' || c=='D'){
				regex = regex + "[0-9]";
			} else {
				regex = regex + c;
			}
		}
		
		regex = regex + "$";

		log.debug(String.format("Checking value '%s' against format '%s', regex '%s'", value, format, regex));
		
		if(!value.matches(regex)){
			log.error(String.format("Time value '%s' does not match format '%s'", value, format));
			return false;
		}
		
		return true;
	}

	/**
	 * Extract values from format and build a timestamp
	 * @param format
	 * @param value
	 * @return
	 */
	public static DateTime getTimestamp(String format, String value) {
		
		/*
		 * YYYY, YY, MM, DD, hh, mm, ss are all indices of where those strings occur in the
		 * format string, they are then replaced with the relevant time from the value string,
		 * formatted as per the format string.
		 * 
		 * If any of 'hour', 'minute', or 'second' are not contained in the format then their value will be 0
		 * 
		 * If 'day', 'month', 'year' are missing their values are replaced with those from the current time
		 * 
		 */
		int YY = -1;
		int YYYY = format.indexOf("YYYY");
		if(YYYY==-1){
			YY = format.indexOf("YY");
		}
		
		int MM = format.indexOf("MM");
		int DD = format.indexOf("DD");
		
		int hh = format.indexOf("hh");
		int mm = format.indexOf("mm");
		int ss = format.indexOf("ss");
		
		int year, month, day, hour, minute, second;		
		DateTime now = DateTime.now();

		if(YYYY != -1){
			year = Integer.parseInt(value.substring(YYYY, YYYY+4));
		} else if(YY != -1) {
			year = Integer.parseInt(value.substring(YY, YY+2));
		} else {
			year = Integer.parseInt(String.valueOf(now.getYear()));
		}

		if(MM != -1){
			month = Integer.parseInt(value.substring(MM, MM+2));
		} else {
			month = Integer.parseInt(String.format("%02d", now.getMonthOfYear()));
		}
		
		if(DD != -1){
			day = Integer.parseInt(value.substring(DD, DD+2));
		} else {
			day = Integer.parseInt(String.format("%02d", now.getDayOfMonth()));
		}
		
		if(hh != -1){
			hour = Integer.parseInt(value.substring(hh, hh+2));
		} else {
			hour = 0;
		}
		
		if(mm != -1){
			minute = Integer.parseInt(value.substring(mm, mm+2));
		} else { 
			minute = 0;
		}
		
		if(ss != -1){
			second = Integer.parseInt(value.substring(ss, ss+2));
		} else {
			second = 0;
		}
		
		DateTime timestamp;
		timestamp = new DateTime(year,month,day,hour,minute,second,DateTimeZone.UTC);
		return timestamp;
	}
}
