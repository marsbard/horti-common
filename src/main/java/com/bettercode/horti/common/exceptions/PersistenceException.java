package com.bettercode.horti.common.exceptions;

import java.io.IOException;

public class PersistenceException extends Exception {

	public PersistenceException(String msg, Exception e) {
		super(msg,e);
	}

	public PersistenceException(String msg) {
		super(msg);
	}

}
