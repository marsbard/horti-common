package com.bettercode.horti.common;

import org.mapdb.DB;
import org.mapdb.DB.HashMapMaker;
import org.mapdb.DBMaker;
import org.mapdb.HTreeMap;

import com.bettercode.horti.common.exceptions.PersistenceException;

public class MapDbPersistenceStrategy implements PersistenceStrategy {

	private static final String TEST_MAP_NAME = null;
	private String persistenceLocation;
	private DB db;
	private HTreeMap<String, String> map;

	@SuppressWarnings("unchecked")
	@Override
	public void connect(String persistenceLocation, String mapName) throws PersistenceException {
		this.persistenceLocation = persistenceLocation;
		db = DBMaker.fileDB(persistenceLocation).make();
		map = (HTreeMap<String, String>) db.hashMap(mapName).createOrOpen();
	}
	
	@Override
	public void close() {
		db.close();
	}
	
	@Override
	public void store(String key, String value) {
		map.put(key, value);
		db.commit();
	}

	@Override
	public String get(String key) {
		return map.get(key);
	}



	public String getPersistenceLocation() {
		return persistenceLocation;
	}

	public void setPersistenceLocation(String persistenceLocation) {
		this.persistenceLocation = persistenceLocation;
	}

}
