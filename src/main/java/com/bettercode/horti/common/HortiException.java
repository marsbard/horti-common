package com.bettercode.horti.common;

public class HortiException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7651219630199043195L;

	public HortiException() {
	}

	public HortiException(String message) {
		super(message);
	}

	public HortiException(Throwable cause) {
		super(cause);
	}

	public HortiException(String message, Throwable cause) {
		super(message, cause);
	}

	public HortiException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
