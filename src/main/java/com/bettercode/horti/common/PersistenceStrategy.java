package com.bettercode.horti.common;

import com.bettercode.horti.common.exceptions.PersistenceException;

public interface PersistenceStrategy {

	void connect(String persistenceLocation, String mapName) throws PersistenceException;
	public void store(String key, String value);
	public String get(String key);
	public void close();
	
}
