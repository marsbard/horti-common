package com.bettercode.horti.component.graphite;

import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;

import com.bettercode.horti.entities.DataIn;

public class QueuedGraphiteProducer extends DefaultGraphiteProducer {

	private Endpoint endpoint;

	public QueuedGraphiteProducer(Endpoint endpoint) {
		super(endpoint);
		this.endpoint = endpoint;
	}

	@Override
	void sendRow(DataIn dataRow) throws Exception {
		throw new Exception("TODO need TODO this, send raw CSV and decode on server maybe using horti-common");
	}

}
