package com.bettercode.horti.component.graphite;

import java.util.List;

import com.bettercode.horti.entities.DataField;
import com.bettercode.horti.graphite.NetClient;

public class GraphiteConfig {

	private GraphiteComponent graphiteComponent;
	private List<DataField> fields;
	private NetClient graphiteClient;

	public NetClient getGraphiteClient() {
		return this.graphiteClient;
	}

	public List<DataField> getFields() {
		return this.fields;
	}

	public GraphiteComponent getGraphiteComponent() {
		return this.graphiteComponent;
	}

	public void setGraphiteComponent(GraphiteComponent graphiteComponent) {
		this.graphiteComponent = graphiteComponent;
	}

	public void setFields(List<DataField> fields) {
		this.fields = fields;
	}

	public void setGraphiteClient(NetClient graphiteClient) {
		this.graphiteClient = graphiteClient;
	}

}
