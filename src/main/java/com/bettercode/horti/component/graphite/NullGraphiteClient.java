package com.bettercode.horti.component.graphite;

import java.io.IOException;

import com.bettercode.horti.graphite.NetClient;

/**
 * A test client with no implementation
 * @author martian
 *
 */
public class NullGraphiteClient implements NetClient {

	@Override
	public void connect(String hostname, int port) throws IOException {
		
	}

	@Override
	public void send(String path, Number value, long currentTime) throws IOException {

	}

	@Override
	public void close() throws IOException {

	}

}
