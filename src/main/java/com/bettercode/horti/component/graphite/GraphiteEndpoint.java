package com.bettercode.horti.component.graphite;

import org.apache.camel.Component;
import org.apache.camel.Consumer;
import org.apache.camel.Processor;
import org.apache.camel.Producer;
import org.apache.camel.impl.DefaultEndpoint;

import com.bettercode.horti.common.Common;
import com.bettercode.horti.graphite.GraphiteClient;

public class GraphiteEndpoint extends DefaultEndpoint {

	private Component component;
	private String endpointUri;

	/*
	 * this gets automatically populated via a camel parameter and if there aren't setters
	 * for all paramaters passed on the uri after the '?' then an exception is thrown
	 */
	private String prefix;
	private GraphiteConfig config;
	public void setPrefix(String prefix){
		this.prefix = prefix;
	}
	
	public GraphiteEndpoint() {
	}

	public GraphiteEndpoint(String endpointUri, Component component, GraphiteConfig config) {
		super(endpointUri, component);
		
		this.config = config;
		this.endpointUri = endpointUri;
		this.component = component;
	}

	@Override
	public Consumer createConsumer(Processor processor) throws Exception {
		throw new UnsupportedOperationException("This endpoint does not support consumers: " + getEndpointUri());
	}

	@Override
	public boolean isSingleton() {
		return false;
	}

	@Override
	public Producer createProducer() throws Exception {
		DirectGraphiteProducer gp = new DirectGraphiteProducer(this);
		
		gp.setConfig(config);
//		gp.setGraphiteClient(config.getGraphiteClient()); 
//		gp.setFields(config.getFields());
//		gp.setComponent(config.getGraphiteComponent());

		return gp;
	}
	
}
