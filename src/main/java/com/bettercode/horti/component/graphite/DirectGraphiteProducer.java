package com.bettercode.horti.component.graphite;

import java.io.IOException;
import java.util.List;

import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultProducer;
import org.apache.log4j.Logger;

import com.bettercode.horti.common.Common;
import com.bettercode.horti.common.HortiException;
import com.bettercode.horti.common.PersistenceStrategy;
import com.bettercode.horti.entities.DataField;
import com.bettercode.horti.entities.DataIn;
import com.bettercode.horti.graphite.NetClient;

public class DirectGraphiteProducer extends DefaultGraphiteProducer {

	// private NetClient cli;
	//
	// private GraphiteComponent component;
	//
	// private List<DataField> fields;
	//
	// private String graphitePrefix;

	private static final Logger log = Logger.getLogger(DirectGraphiteProducer.class);
	private static final String GRAPHITE_CACHE = "graphite-cache";
	private PersistenceStrategy persistenceStrategy;
	private String persistenceLocation;

	public DirectGraphiteProducer(GraphiteEndpoint graphiteEndpoint) {
		super(graphiteEndpoint);
	}

	void setPersistenceStrategy(PersistenceStrategy strategy, String persistenceLocation) {
		this.persistenceStrategy = strategy;
		this.persistenceLocation = persistenceLocation;
	}

	void sendRow(DataIn dataRow) throws Exception {

		NetClient cli = config.getGraphiteClient();

		GraphiteComponent component = config.getGraphiteComponent();

		String graphitePrefix = Common.GRAPHITE_PREFIX;

		List<DataField> fields = config.getFields();

		if (cli == null) {
			throw new HortiException(String.format("Error in '%s:sendRow()' - need to call setGraphiteClient() first",
					this.getClass().getName()));
		}

		/*
		 * if no valid time stamp don't attempt to send (probable previous
		 * exception in DataIn)
		 */
		if (dataRow.getTimestamp() == null) {
			return;
		}

		try {
			log.debug(String.format("Connecting to %s %s", component.getGraphiteHost(), component.getGraphitePort()));
			cli.connect(component.getGraphiteHost(), component.getGraphitePort());
			sendRowDynamic(cli, dataRow, fields, graphitePrefix);

			if (persistenceStrategy != null) {
				persistenceStrategy.connect(persistenceLocation, GRAPHITE_CACHE);
			}
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				cli.close();
			} catch (IOException e) {
			}
		}
	}

	private void sendRowDynamic(NetClient cli, DataIn dataRow, List<DataField> fields, String graphitePrefix)
			throws HortiException, IOException {
		long time = dataRow.getTimestamp().getMillis() / 1000;

		for (DataField field : fields) {

			// only one timestamp field allowed and it's not sent as a datum.
			if (field.getType().equals("time")) {
				continue;
			}

			if (dataRow.getValue(field.getName()) == null) {
				log.error(String.format("There is no field called '%s' in the data row", field.getName()));

			}
			String value = dataRow.getValue(field.getName());

			Number numValue = null;

			switch (field.getType()) {

			case "int":
				numValue = Integer.parseInt(value);
				break;

			case "1dp":
				numValue = Float.parseFloat(value) / 10;
				break;

			case "bool":
				numValue = Integer.parseInt(value);
				break;

			default:
				throw new HortiException(String.format("Unknown type '%s'", field.getType()));
			}

			cli.send(graphitePrefix + "." + field.getName(), numValue, time);
		}

	}

}
