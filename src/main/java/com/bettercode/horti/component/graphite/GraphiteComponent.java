package com.bettercode.horti.component.graphite;

import java.util.Map;

import org.apache.camel.Endpoint;
import org.apache.camel.impl.DefaultComponent;
import org.apache.log4j.Logger;

import com.bettercode.horti.common.Common;
import com.bettercode.horti.common.HortiException;


public class GraphiteComponent extends DefaultComponent {
	
	public String getGraphiteHost() {
		return graphiteHost;
	}

	public int getGraphitePort() {
		return graphitePort;
	}

	@SuppressWarnings("unused")
	private static final Logger log = Logger.getLogger(GraphiteComponent.class);

	String graphiteHost = null;
	int graphitePort = 2003;

	private GraphiteConfig config;

	@Override
	protected Endpoint createEndpoint(String uri, String remaining, Map<String, Object> parameters) throws Exception {
		
		graphiteHost = remaining.split(":")[0];
		String rest = remaining.split(":")[1] ;
		graphitePort = rest != null ? Integer.parseInt(rest): Common.GRAPHITE_DEFAULT_PORT;
		
		if(config==null){
			String msg = "Need to call setConfig() first";
			log.error(msg);
			throw new HortiException(msg );
		}
		
        Endpoint endpoint = (Endpoint) new GraphiteEndpoint(uri, this, config);
        setProperties(endpoint, parameters);
        return endpoint;
	}

	public GraphiteConfig getConfig() {
		return config;
	}

	public void setConfig(GraphiteConfig config) {
		this.config = config;
	}

}
