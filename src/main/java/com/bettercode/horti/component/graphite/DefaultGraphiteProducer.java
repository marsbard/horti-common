package com.bettercode.horti.component.graphite;

import java.util.List;

import org.apache.camel.Endpoint;
import org.apache.camel.Exchange;
import org.apache.camel.impl.DefaultProducer;

import com.bettercode.horti.common.HortiException;
import com.bettercode.horti.entities.DataIn;

public abstract class DefaultGraphiteProducer extends DefaultProducer {

	protected GraphiteConfig config;

	public DefaultGraphiteProducer(Endpoint endpoint) {
		super(endpoint);
	}
	
	@SuppressWarnings("unchecked")
	public void process(Exchange exchange) throws Exception {

		List<List<String>> body = getBody(exchange);

		if(config==null){
			String msg = "Need to call setConfig() first";
			log.error(msg);
			throw new HortiException(msg );
		}
		

		int lineNo = 1;
		for (List<String> row : body) {
			DataIn dataRow = new DataIn(row, config.getFields());
			log.debug(String.format("%s", dataRow));

			try {
				sendRow(dataRow);
			} catch (Exception e) {
				String inputPath = (String) exchange.getIn().getHeader("CamelFileAbsolutePath");
				log.error(e.getClass().getName() + " Exception sending data to graphite: " + e.getMessage() + " in "
						+ inputPath + " at line " + lineNo);
			}
			lineNo++;
		}
	}

	abstract void sendRow(DataIn dataRow) throws Exception;

	protected List<List<String>> getBody(Exchange exchange) {
		/*
		 * A row is represented by a List<String> and so a number of rows becomes
		 * a list of those, i.e. List<List<String>>
		 */
		@SuppressWarnings("unchecked")
		List<List<String>> body = (List<List<String>>) exchange.getIn().getBody();
		return body;
	}

	public GraphiteConfig getConfig() {
		return config;
	}

	public void setConfig(GraphiteConfig config) {
		this.config = config;
	}


}
