package com.bettercode.horti.graphite;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

import javax.net.SocketFactory;

import org.apache.log4j.Logger;

public class GraphiteClient implements NetClient {

	private InetSocketAddress serverAddress;
	private SocketFactory socketFactory;
	private Socket socket;
	private BufferedWriter writer;

	private static final Logger log = Logger.getLogger(GraphiteClient.class);

	GraphiteClient() {
		this.socketFactory = SocketFactory.getDefault();
	}

	/* (non-Javadoc)
	 * @see com.bettercode.horti.graphite.NetClient#connect(java.lang.String, int)
	 */
	public void connect(String hostname, int port) throws IOException {
		if (socket != null) {
			throw new IllegalStateException("Connection has already been established");
		}

		this.serverAddress = new InetSocketAddress(hostname, port);

		socket = socketFactory.createSocket(serverAddress.getHostName(), serverAddress.getPort());
		writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
	}

	/* (non-Javadoc)
	 * @see com.bettercode.horti.graphite.NetClient#send(java.lang.String, java.lang.Number, long)
	 */
	public void send(String path, Number value, long currentTime) throws IOException {

		if (value == null) {
			value = 0;
		}
		log.debug(String.format("%s: %s %s", path, value.toString(), Long.toString(currentTime)));

		writer.write(path);
		writer.write(' ');
		writer.write(value.toString());
		writer.write(' ');
		writer.write(Long.toString(currentTime));
		writer.write('\n');
		writer.flush();
	}

	/* (non-Javadoc)
	 * @see com.bettercode.horti.graphite.NetClient#close()
	 */
	public void close() throws IOException {
		if (socket == null) {
			throw new IllegalStateException("Connection is not established or closed");
		}

		writer.close();
		socket.close();

		writer = null;
		socket = null;
	}

	public InetSocketAddress getServerAddress() {
		return serverAddress;
	}

	public SocketFactory getSocketFactory() {
		return socketFactory;
	}

}
