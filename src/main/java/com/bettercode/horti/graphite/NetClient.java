package com.bettercode.horti.graphite;

import java.io.IOException;

public interface NetClient {

	void connect(String hostname, int port) throws IOException;

	void send(String path, Number value, long currentTime) throws IOException;

	void close() throws IOException;

}