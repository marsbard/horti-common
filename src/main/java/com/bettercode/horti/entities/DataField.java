package com.bettercode.horti.entities;

import java.util.Map;

public class DataField {

	private String name;
	private String description;
	private String type;   // int | 1dp | bool | time
	private String format; // only used when type=='time'
	
	public DataField() {
	}

	public DataField(Map<String,String> field) {
		this.name = field.get("name");
		this.description = field.get("descr");
		this.type = field.get("type");
		this.format = field.get("format");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	
	
}
