package com.bettercode.horti.entities;


public class DataValue {

	public DataField field; // related field definition
	public String value;
	
	
	public DataField getField() {
		return field;
	}
	public void setField(DataField field) {
		this.field = field;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

}
