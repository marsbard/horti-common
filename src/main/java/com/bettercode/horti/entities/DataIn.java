package com.bettercode.horti.entities;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import org.joda.time.DateTime;

import com.bettercode.horti.common.TimeUtil;

public class DataIn {

	final private static Logger log = Logger.getLogger(DataIn.class);

	private DateTime timestamp;

	private Map<String, DataValue> values;

	public DataIn(List<String> row, List<DataField> fields) {
		dynamicConstructor(row, fields);
	}

	/*
	 * Constructor using fields from the supplied YAML file (see FieldLoader,
	 * Data1FieldLoader)
	 */
	private void dynamicConstructor(List<String> row, List<DataField> fields) {
		values = new HashMap<String, DataValue>();
		String format = "";
		int col = 1; // first col(0) is 'R' command
		for (DataField field : fields) {
			DataValue value = new DataValue();
			value.setField(field);
			value.setValue(row.get(col++));
			values.put(field.getName(), value);
			if (field.getType() == "time") {
				format = field.getFormat();
			}
		}

		timestamp = TimeUtil.getTimestamp(format, values.get("time").getValue());
	}

	public DataIn() {
	}

	public String getValue(String name) {
		return values.get(name).getValue();
	}

	public DateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(DateTime timestamp) {
		this.timestamp = timestamp;
	}

	public Map<String, DataValue> getValues() {
		return values;
	}

	public void setValues(Map<String, DataValue> values) {
		this.values = values;
	}

	public static Logger getLog() {
		return log;
	}

	
	
}
