package com.bettercode.horti.component.graphite;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import com.bettercode.horti.common.HortiException;
import com.bettercode.horti.entities.DataField;
import com.bettercode.horti.entities.DataIn;
import com.bettercode.horti.graphite.NetClient;

public class DirectGraphiteProducerTest {

	
	private DirectGraphiteProducer producer;
	
	@Before
	public void setup(){
		producer = new DirectGraphiteProducer(null);
	}
	
	@Test(expected = HortiException.class)
	public void testSendRowWithoutSettingClient() throws Exception {
		GraphiteComponent component = new GraphiteComponent();
		NetClient client = null;
		List<DataField> fields = new ArrayList<DataField>();
		
		GraphiteConfig config = new GraphiteConfig();
		config.setGraphiteClient(client);
		config.setGraphiteComponent(component);
		config.setFields(fields);
		
		producer.setConfig(config);
		
		producer.sendRow(new DataIn());
	}

	@Test
	public void testSendRowWithClient() throws Exception {
		GraphiteComponent component = new GraphiteComponent();
		NetClient client = new NullGraphiteClient();
		List<DataField> fields = new ArrayList<DataField>();
		
		GraphiteConfig config = new GraphiteConfig();
		config.setGraphiteClient(client);
		config.setGraphiteComponent(component);
		config.setFields(fields);

		producer.setConfig(config);
		
		DataIn row = new DataIn();
		DateTime time = new DateTime();
		row.setTimestamp(time);
		producer.sendRow(row);
	}
}
