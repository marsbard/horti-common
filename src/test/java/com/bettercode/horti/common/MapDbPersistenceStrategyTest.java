package com.bettercode.horti.common;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import com.bettercode.horti.common.exceptions.PersistenceException;

public class MapDbPersistenceStrategyTest {

	private static final String TEST_MAP_NAME = "test-map";
	private PersistenceStrategy strategy;

	@Before
	public void setup() {
	}

	@Rule
	public TemporaryFolder testFolder = new TemporaryFolder();

	private void doSimpleTest() throws PersistenceException {
		String key = "test_key";
		String value = "TEST test TEST value";
		strategy.store(key, value);
		assertEquals(value, strategy.get(key));
	}

	@Test
	public void basicTest() throws PersistenceException {
		strategy = new MemTestingPersistenceStrategy();
		strategy.connect("", TEST_MAP_NAME);

		doSimpleTest();
	}

	@Test
	public void mapDbTest() throws PersistenceException {
		strategy = new MapDbPersistenceStrategy();
		strategy.connect(testFolder.getRoot() + "/mapdb-test", TEST_MAP_NAME);

		doSimpleTest();

	}

}
