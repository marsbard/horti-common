package com.bettercode.horti.common;

import java.util.HashMap;

import com.bettercode.horti.common.exceptions.PersistenceException;

public class MemTestingPersistenceStrategy implements PersistenceStrategy {


	private HashMap<String, String> store;

	public MemTestingPersistenceStrategy(){
		store = new HashMap<String, String>();
	}
	

	@Override
	public void store(String key, String value) {
		store.put(key, value);
	}

	@Override
	public void close() {

	}

	@Override
	public String get(String key) {
		return store.get(key);
	}


	@Override
	public void connect(String persistenceLocation, String mapName) throws PersistenceException {
		
	}


}
